require('./common/bootstrap');

import Vue              from 'vue'
import Notifications    from 'vue-notification'
import Velocity         from 'velocity-animate'

import App              from './App.vue';

import router           from './router';
import config           from './config';
import http             from './helpers/http'

Vue.use(Notifications, {Velocity});

Vue.prototype.$http = http;

const app = new Vue({
    router,
    template: `<app></app>`,
    components: { App },
    data:{
        message: config.message
    }
}).$mount('#app');