import 'nprogress/nprogress.css'

import Axios from 'axios';
import NProgress from 'nprogress'

const http = Axios.create({
    baseURL: window.api_url,
    headers: {
        'X-CSRF-TOKEN': window.csrf_token,
        'X-Requested-With': 'XMLHttpRequest',
    }
});

// Add a request interceptor
http.interceptors.request.use(function (request) {
    NProgress.start();
    return request;
}, function (error) {
    NProgress.done();
    return Promise.reject(error);
});

// Add a response interceptor
http.interceptors.response.use(function (response) {
    NProgress.done();
    return response;
}, function (error) {
    NProgress.done();
    return Promise.reject(error);
});

export default http