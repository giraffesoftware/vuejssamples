const config = {
    message: {
        error: {
            webcam: {
                permissionDenied: 'Can not access webcam, please allow access to use the web camera and reload page'
            }
        },
        forms: {
            service: {
                required: 'You did not choose a service to compare faces'
            }
        }
    }
};

export default config