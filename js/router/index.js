import Vue from "vue"
import VueRouter from 'vue-router'
import NProgress from 'nprogress'

import Home from '../views/Home.vue';
import Recognition from '../views/Recognition.vue';
import Comparison from '../views/Comparison.vue';
import NotFound from '../views/NotFound.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [{
        path: '/',
        name: 'home',
        component: Home
    },{
        path: '/recognition',
        name: 'recognition',
        component: Recognition,
        header: 'Recognition of documents',
    },{
        path: '/comparison',
        name: 'comparison',
        component: Comparison,
        header: 'Comparison faces',
    },{
        path: '/not-found',
        name: 'notFound',
        component: NotFound
    },{
        path: '*',
        component: NotFound
    }]
});

router.beforeEach((to, from, next) => {
    NProgress.start();
    next();
});

router.afterEach((to, from) => {
    NProgress.done();
});

export default router